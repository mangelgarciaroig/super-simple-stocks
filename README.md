# SuperSimpleStocks
Example Assignment – SuperSimpleStocks

#Requirements

* Provide working source code that will:-
  * For a given stock,
    * Given any price as input, calculate the dividend yield
    * Given any price as input, calculate the P/E Ratio
    * Record a trade, with timestamp, quantity of shares, buy or sell indicator and
    * Calculate Volume Weighted Stock Price based on trades in past 15 minutes
  * Calculate the GBCE All Share Index using the geometric mean of prices for all stocks traded price

#Constraints & Notes

* Written in one of these languages: Java, C#, C++, Python
* No database or GUI is required, all data need only be held in memory
* No prior knowledge of stock markets or trading is required – all formulas are provided below.

#How to use:
This is a maven project, so you can run these 2 goals:
* mvn test -> to execute the unit tests.
* mvn package -> to generate the executable jar.

To run the program just run:
* java -jar target/super-simple-stocks-1.0-SNAPSHOT.jar

#Classes
* org.miguelangelgarciaroig.supersimplestoks.app.App -> Main application entry point
* org.miguelangelgarciaroig.supersimplestoks.app.service.TimeService -> Application service to retrieve the system current time
* org.miguelangelgarciaroig.supersimplestoks.domain.entity.Stock -> Base class for stock entities
* org.miguelangelgarciaroig.supersimplestoks.domain.entity.CommonStock -> Common stock entity
* org.miguelangelgarciaroig.supersimplestoks.domain.entity.CommonStock -> PreferredStock stock entity
* org.miguelangelgarciaroig.supersimplestoks.domain.entity.Trade -> Class modeling a trade
* org.miguelangelgarciaroig.supersimplestoks.domain.entity.TradeCollection -> Trade collection encapsulation
* org.miguelangelgarciaroig.supersimplestoks.domain.service.GBCEShareIndexService -> Domain service to calculate the shares index
* org.miguelangelgarciaroig.supersimplestoks.persistence.InMemoryStockRepository -> Persistence stock repository (to load stocks,
for this exercise purposes Stocks are hold in main memory)

#Packages
* org.miguelangelgarciaroig.supersimplestoks.app -> Main application package
* org.miguelangelgarciaroig.supersimplestoks.app.service -> Application level services
* org.miguelangelgarciaroig.supersimplestoks.domain.entity -> Domain entities package
* org.miguelangelgarciaroig.supersimplestoks.domain.service -> Domain level services
* org.miguelangelgarciaroig.supersimplestoks.persistence -> Persistence related classes