package org.miguelangelgarciaroig.supersimplestoks.app;

import org.miguelangelgarciaroig.supersimplestoks.app.service.TimeService;
import org.miguelangelgarciaroig.supersimplestoks.domain.entity.Stock;
import org.miguelangelgarciaroig.supersimplestoks.domain.service.GBCEShareIndexService;
import org.miguelangelgarciaroig.supersimplestoks.persistence.InMemoryStockRepository;

import java.util.Collection;
import java.util.logging.Logger;

/**
 * Application main entry point
 *
 */
public class App 
{
    private final static Logger logger = Logger.getLogger(App.class.getName());

    private final static int PAST_MINUTES_TO_SEARCH_FOR_TRADES = 15;

    public static void main( final String... args )
    {
        (new App()).run();
    }

    public void run(){

        final Collection<Stock> allStocks = loadStockRepository().findAll();

        logger.info("SuperSimpleStocks Application. Miguel Ángel García Roig (magarcia_work@ono.com)");

        logger.info(String.format("Current datetime: %s", TimeService.get().currentTime()));

        logger.info(String.format("The GBCE share index for all stocks is: %f", calculateGBCEShareIndex(allStocks)));

        for (final Stock currentStock : allStocks){

            logger.info(String.format("Loaded stock: %s", currentStock.toString()));

            logger.info(String.format("Stock PE-ratio: %f", currentStock.calculatePERatio()));

            logger.info(String.format("Stock price from the trades registered in the past minutes: %f",
                    calculatePriceFromTradesInThePastMinutes(currentStock)));
        }
    }

    private double calculatePriceFromTradesInThePastMinutes(final Stock stock){
        return stock.calculateTradePrice(TimeService.get().currentTime(), PAST_MINUTES_TO_SEARCH_FOR_TRADES);
    }

    private double calculateGBCEShareIndex(final Collection<Stock> allStocks) {

        return new GBCEShareIndexService().calculateShareIndex(allStocks);
    }

    private InMemoryStockRepository loadStockRepository(){

        return new  InMemoryStockRepository(TimeService.get().currentTime());
    }
}
