package org.miguelangelgarciaroig.supersimplestoks.app.service;

import java.util.Date;

/**
 * Note: In a first view, this class may seem that it has no sense. But access to external resources (like getting the system time)
 * should always be encapsulated, and not to make any assumption.
 *
 * Passing the system time as a parameter into domain objects has a lots of benefits; the most immediate  one could
 * be to facilitate the unit tests.
 *
 * For the purposes of this exercise, this simple implementation is enough
 */
public class TimeService {

    private final static TimeService timeService = new TimeService(new Date());

    public static TimeService get(){
        return timeService;
    }

    private final Date now;

    private TimeService(final Date now){
        this.now = now;
    }

    public Date currentTime(){
        return this.now;
    }
}
