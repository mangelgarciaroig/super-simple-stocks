package org.miguelangelgarciaroig.supersimplestoks.domain.entity;


import com.google.common.base.Throwables;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Date;
import java.util.logging.Logger;
import java.util.logging.Level;

public class Trade implements Cloneable {

    private final static Logger logger = Logger.getLogger(Trade.class.getName());

    public enum TradeType {BUY, SELL};

    private final TradeType type;

    private Date timestamp;

    private Integer quantityOfShares;

    private Double unitPrice;

    public Trade(final TradeType type, final Date timestamp, final Integer quantityOfShares, final Double unitPrice){

        this.type = type;

        setTimestamp(timestamp);
        setQuantityOfShares(quantityOfShares);
        setUnitPrice(unitPrice);
    }

    public Double calculateTotalPrice(){
        return getQuantityOfShares() * getUnitPrice();
    }

    public final TradeType getType() {
        return type;
    }

    public final Date getTimestamp() {
        return timestamp;
    }

    private void setTimestamp(final Date timestamp) {
        this.timestamp = timestamp;
    }

    public final Integer getQuantityOfShares() {
        return quantityOfShares;
    }

    private void setQuantityOfShares(final Integer quantityOfShares) {
        this.quantityOfShares = quantityOfShares;
    }

    public final Double getUnitPrice() {
        return unitPrice;
    }

    private void setUnitPrice(final Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public boolean equals(final Object other){

        boolean weAreEquals = false;

        if (other instanceof Trade){

            weAreEquals = EqualsBuilder.reflectionEquals(this, other);
        }

        return weAreEquals;
    }

    @Override
    public int hashCode(){

        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString(){

        return String.format("%s {type: %s, timestamp: %s, quantity: %s, unitPrice: %f}", getClass().getSimpleName(),
            getType().toString(), getTimestamp().toString(), getQuantityOfShares(), getUnitPrice());
    }

    public Trade clone()  {

        Trade cloned = null;

        try {
            cloned = (Trade) super.clone();

        } catch (CloneNotSupportedException impossibleToHappenError) {

            // NOTE: It's not possible that a CloneNotSupportedException happens, so we opt for propagating the exception at runtime
            // (the compiler forces us to catch the exception)
            // If a the error were really possible, we would have defined a custom exception and we would have declared it
            // in the method throws section
            logger.log(Level.SEVERE, "Unable to clone trade", impossibleToHappenError);
            Throwables.propagate(impossibleToHappenError);
        }

        return cloned;
    }
}
