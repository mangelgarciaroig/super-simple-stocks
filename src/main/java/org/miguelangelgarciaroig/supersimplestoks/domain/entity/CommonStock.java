package org.miguelangelgarciaroig.supersimplestoks.domain.entity;


public class CommonStock extends Stock {

    public CommonStock(final String id, final int lastDividend, final int parValue, final double tickerPrice){

        super(id, StockType.COMMON);

        setLastDividend(lastDividend);
        setParValue(parValue);
        setTickerPrice(tickerPrice);
    }

    @Override
    public double calculateDividendYield() {

        return getLastDividend() / getTickerPrice();
    }

    @Override
    public String toString(){
        return String.format("%s {id: %s, type: %s, parValue: %d, lastDividend: %d, tickerPrice: %f, trades: %s}", getClass().getSimpleName(),
                getId(), getType().toString(), getParValue(), getLastDividend(), getTickerPrice(), getTrades().toString());
    }
}
