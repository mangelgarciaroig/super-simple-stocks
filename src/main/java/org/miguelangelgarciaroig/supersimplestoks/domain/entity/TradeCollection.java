package org.miguelangelgarciaroig.supersimplestoks.domain.entity;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public class TradeCollection {

    private final Collection<Trade> trades = new ArrayList<>();

    public int count(){
        return trades.size();
    }

    public boolean isEmpty(){
        return count() < 1;
    }

    public double multiplyAllPrices(){

        if (isEmpty()) return 0.0;

        double pricesMultiplication = 1.0;

        final Iterator<Trade> tradesIt = iterator();

        while (tradesIt.hasNext()){
            pricesMultiplication*= tradesIt.next().calculateTotalPrice();
        }

        return pricesMultiplication;
    }

    TradeCollection add(final Trade toBeAdded){

        trades.add(toBeAdded.clone());

        return this;
    }

    double calculatePrice(final Date from, final Date to){

        final Iterator<Trade> filteredTradesIt = filter(from, to);

        double accumulatedNonWeightedPrice = 0.0;
        double accumulatedQuantity = 0.0;

        while (filteredTradesIt.hasNext()){

            final Trade currentFilteredTrade = filteredTradesIt.next();

            accumulatedNonWeightedPrice+= currentFilteredTrade.calculateTotalPrice();
            accumulatedQuantity+= currentFilteredTrade.getQuantityOfShares();
        }

        return accumulatedNonWeightedPrice / accumulatedQuantity;
    }

    Iterator<Trade> iterator(){
        return trades.iterator();
    }

    @Override
    public String toString(){

        final StringBuilder builder = new StringBuilder(getClass().getSimpleName());

        builder.append(" {\n");

        final Iterator<Trade> allTradesIt = iterator();
        while (allTradesIt.hasNext()){

            builder.append("\t");
            builder.append(allTradesIt.next().toString());
            builder.append("\n");
        }

        builder.append("}\n");

        return builder.toString();
    }

    private Iterator<Trade> filter(Date from, Date to){

        if (to.before(from)){

            Date temp = to;
            to = from;
            from = temp;
        }

        final Collection<Trade> filtered = new ArrayList<>();

        final Iterator<Trade> tradesIt = iterator();
        while (tradesIt.hasNext()){

            final Trade currentTrade = tradesIt.next();

            if (belongsToFilter(currentTrade, from, to)){
                filtered.add(currentTrade);
            }
        }

        return filtered.iterator();
    }

    private boolean belongsToFilter(final Trade toBeChecked, final Date from, final Date to){

        final Date toBeCheckedTimestamp = toBeChecked.getTimestamp();

        return isBeforeOrEquals(from, toBeCheckedTimestamp) && isAfterOrEquals(toBeCheckedTimestamp, to);
    }

    private boolean isAfterOrEquals(final Date minorDate, final Date majorDate){
        return  majorDate.after(minorDate) || minorDate.equals(majorDate);
    }

    private boolean isBeforeOrEquals(final Date minorDate, final Date majorDate){
        return  minorDate.before(majorDate) || minorDate.equals(majorDate);
    }
}
