package org.miguelangelgarciaroig.supersimplestoks.domain.entity;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

public abstract class Stock {

    public enum StockType {COMMON, PREFERRED};

    private final String id;

    private final StockType type;

    private Integer lastDividend;

    private Double fixedDividend;

    private Integer parValue;

    private Double tickerPrice;

    private final TradeCollection trades = new TradeCollection();

    Stock(final String id, final StockType type) {

        this.id = id;
        this.type = type;
    }

    public abstract double calculateDividendYield();

    public double calculatePERatio() {
        return getTickerPrice() / calculateDividendYield();
    }

    public double calculateTradePrice(final Date now, final int minutesBefore){

        final Date from = subtractMinutes(now, minutesBefore);

        return trades.calculatePrice(from, now);
    }

    public Stock addTrade(final Trade toBeAdded){

        trades.add(toBeAdded.clone());

        return this;
    }

    public Iterator<Trade> tradesIterator(){
        return trades.iterator();
    }

    public final String getId() {
        return id;
    }

    public final StockType getType() {
        return type;
    }

    public final Integer getLastDividend() {
        return lastDividend;
    }

    protected final void setLastDividend(final Integer lastDividend) {
        this.lastDividend = lastDividend;
    }

    public final Double getFixedDividend() {
        return fixedDividend;
    }

    protected final void setFixedDividend(final Double fixedDividend) {
        this.fixedDividend = fixedDividend;
    }

    public final Integer getParValue() {
        return parValue;
    }

    protected final void setParValue(final Integer parValue) {
        this.parValue = parValue;
    }

    public final Double getTickerPrice() {
        return tickerPrice;
    }

    protected final void setTickerPrice(final Double tickerPrice) {
        this.tickerPrice = tickerPrice;
    }

    public TradeCollection getTrades() {
        return trades;
    }

    private Date subtractMinutes(final Date now, final int minutesBefore) {

        final Calendar calendar = new GregorianCalendar();
        calendar.setTime(now);
        calendar.add(Calendar.MINUTE, -Math.abs(minutesBefore));

        return calendar.getTime();
    }
}
