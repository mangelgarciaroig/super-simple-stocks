package org.miguelangelgarciaroig.supersimplestoks.domain.entity;


public class PreferredStock extends Stock {

    public PreferredStock(final String id, final int parValue, final double fixedDividend, final double tickerPrice){

        super(id, StockType.PREFERRED);

        setParValue(parValue);
        setFixedDividend(fixedDividend);
        setTickerPrice(tickerPrice);
    }

    @Override
    public double calculateDividendYield() {

        return (getFixedDividend() * getParValue()) / getTickerPrice();
    }

    @Override
    public String toString(){
        return String.format("%s {id: %s, type: %s, parValue: %d, fixedDividend: %f, tickerPrice: %f, trades: %s}", getClass().getSimpleName(),
                getId(), getType().toString(), getParValue(), getFixedDividend(), getTickerPrice(), getTrades().toString());
    }
}
