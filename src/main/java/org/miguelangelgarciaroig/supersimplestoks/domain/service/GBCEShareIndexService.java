package org.miguelangelgarciaroig.supersimplestoks.domain.service;

import java.util.Collection;

import org.miguelangelgarciaroig.supersimplestoks.domain.entity.Stock;
import org.miguelangelgarciaroig.supersimplestoks.domain.entity.TradeCollection;

public class GBCEShareIndexService {

    public double calculateShareIndex(final Collection<Stock> stocks){

        double shareIndex = 0.0;

        double pricesMultiplication = 1.0;
        int numberOfTrades = 0;

        for (final Stock currentStock : stocks){

            final TradeCollection currentTrades = currentStock.getTrades();

            if (!currentTrades.isEmpty()){
                pricesMultiplication*= currentTrades.multiplyAllPrices();
                numberOfTrades++;
            }
        }

        if (numberOfTrades > 0){
            shareIndex = Math.pow(pricesMultiplication, 1.0 / numberOfTrades);
        }

        return shareIndex;
    }
}
