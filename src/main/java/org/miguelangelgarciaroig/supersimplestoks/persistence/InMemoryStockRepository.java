package org.miguelangelgarciaroig.supersimplestoks.persistence;

import org.miguelangelgarciaroig.supersimplestoks.domain.entity.CommonStock;
import org.miguelangelgarciaroig.supersimplestoks.domain.entity.PreferredStock;
import org.miguelangelgarciaroig.supersimplestoks.domain.entity.Stock;
import org.miguelangelgarciaroig.supersimplestoks.domain.entity.Trade;

import java.util.*;

/**
 * Note: In a more sophisticated implementation, we should have an interface to hold all this public methods.
 * Tipically an 'InMemory' repository must be under the test sources
 */
public class InMemoryStockRepository {

    private final List<Stock> allStocks = new ArrayList<>();

    private final Calendar calendar = new GregorianCalendar();

    public InMemoryStockRepository(final Date now){

        loadData(now);
    }

    public Collection<Stock> findAll(){

        return new ArrayList<Stock>(allStocks);
    }

    private void loadData(Date now) {
        final Trade oneHourBeforeTrade = createOneHourBeforeTrade(Trade.TradeType.BUY, now, 3, 1.2);
        final Trade tenMinutesBeforeTrade = createTenMinutesBeforeTrade(Trade.TradeType.SELL, now, 2, 4.1);
        final Trade oneMinuteBeforeTrade = createOneMinuteBeforeTrade(Trade.TradeType.BUY, now, 1, 1.2);

        final Stock teaStock = new CommonStock("TEA", 0, 100, 3.2);
        final Stock popStock = new CommonStock("POP", 8, 100, 2.2);
        final Stock aleStock = new CommonStock("ALE", 23, 60, 1.1);
        final Stock ginStock = new PreferredStock("GIN", 100, 0.02, 1.8);
        final Stock joeStock = new CommonStock("JOE", 13, 250, 3.5);

        final Stock[] preLoadedStocks = {teaStock, popStock, aleStock, ginStock, joeStock};

        for (final Stock currentStock : preLoadedStocks){

            currentStock
                    .addTrade(oneHourBeforeTrade)
                    .addTrade(tenMinutesBeforeTrade)
                    .addTrade(oneMinuteBeforeTrade);

            allStocks.add(currentStock);
        }
    }

    private Trade createOneHourBeforeTrade(final Trade.TradeType type, final Date now, final int quantityOfShares, final Double unitPrice){

        return new Trade(type, minutesBefore(now, 60), quantityOfShares, unitPrice);
    }

    private Trade createTenMinutesBeforeTrade(final Trade.TradeType type, final Date now, final int quantityOfShares, final Double unitPrice){

        return new Trade(type, minutesBefore(now, 10), quantityOfShares, unitPrice);
    }

    private Trade createOneMinuteBeforeTrade(final Trade.TradeType type, final Date now, final int quantityOfShares, final Double unitPrice){

        return new Trade(type, minutesBefore(now, 1), quantityOfShares, unitPrice);
    }

    private Date minutesBefore(final Date now, final int minutesBefore){

        calendar.setTime(now);
        calendar.add(Calendar.MINUTE, -Math.abs(minutesBefore));

        return calendar.getTime();
    }
}
