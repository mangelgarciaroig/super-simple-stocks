package org.miguelangelgarciaroig.supersimplestoks.domain.entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

class BaseStockTest {

    protected void addAndRetrieveTradesTest(final Stock stock){

        final Date now = new Date();

        final Trade firstTrade = new Trade(Trade.TradeType.BUY, now, 2, 10.0);
        final Trade secondTrade = new Trade(Trade.TradeType.SELL, now, 1, 5.0);

        stock.addTrade(firstTrade);
        stock.addTrade(secondTrade);

        final Collection<Trade> trades = tradesFrom(stock);

        int expectedNumberOfTrades = 2;
        int obtainedNumberOfTrades = trades.size();

        assertEquals("Unexpected number of trades in stock", expectedNumberOfTrades, obtainedNumberOfTrades);
        assertTrue("Expected trade to be contained in stock", trades.contains(firstTrade));
        assertTrue("Expected trade to be contained in stock", trades.contains(secondTrade));
    }

    protected void calculateTradePricesTest(final Stock stock) throws ParseException {

        final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        final Date firstDate = formatter.parse("2016-01-01 00:00:00");
        final Date secondDate = formatter.parse("2016-01-01 12:00:00");
        final Date thirdDate = formatter.parse("2016-01-02 05:00:00");
        final Date fourthDate = formatter.parse("2016-01-02 23:00:00");
        final Date fifthDate = formatter.parse("2016-01-02 23:59:59");

        final Trade firstTrade = new Trade(Trade.TradeType.BUY, firstDate, 1, 12.3);
        final Trade secondTrade = new Trade(Trade.TradeType.SELL, secondDate, 23, 50.3);
        final Trade thirdTrade = new Trade(Trade.TradeType.BUY, thirdDate, 4, 100.2);
        final Trade fourthTrade = new Trade(Trade.TradeType.SELL, fourthDate, 2, 23.1);
        final Trade fifthTrade = new Trade(Trade.TradeType.BUY, fifthDate, 8, 1.2);

        stock.addTrade(firstTrade)
                .addTrade(secondTrade)
                .addTrade(thirdTrade)
                .addTrade(fourthTrade)
                .addTrade(fifthTrade);

        int minutesBefore = 60;
        final Double expectedPrice = calculateExpectedPrice(fourthTrade, fifthTrade);
        final Double obtainedPrice = stock.calculateTradePrice(fifthDate, minutesBefore);

        assertEquals("Unexpected total price for stock was found", expectedPrice, obtainedPrice);
    }

    private double calculateExpectedPrice(final Trade ...trades) {

        double totalUnweightedPrice = 0.0;
        double totalQuantity = 0.0;

        for (final Trade currentTrade : trades){
            totalUnweightedPrice+= currentTrade.getUnitPrice() * currentTrade.getQuantityOfShares();
            totalQuantity+= currentTrade.getQuantityOfShares();
        }

        return totalUnweightedPrice / totalQuantity;
    }

    private Collection<Trade> tradesFrom(final Stock stock){

        final Collection<Trade> trades = new ArrayList<>();

        final Iterator<Trade> tradesIt = stock.tradesIterator();

        while (tradesIt.hasNext()){
            trades.add(tradesIt.next());
        }

        return trades;
    }
}
