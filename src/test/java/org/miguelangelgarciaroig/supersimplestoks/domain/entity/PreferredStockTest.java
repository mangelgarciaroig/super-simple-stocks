package org.miguelangelgarciaroig.supersimplestoks.domain.entity;


import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

import static org.junit.Assert.assertEquals;

public class PreferredStockTest extends BaseStockTest {

    private final String stockId = "Stock1";
    private final Double fixedDividend = 5.3;
    private final Integer parValue = 2;
    private final Double tickerPrice = 10.2;

    private PreferredStock stock;

    @Before
    public void setUp(){
        stock = new PreferredStock(stockId, parValue, fixedDividend, tickerPrice);
    }

    @Test
    public void constructorTest(){

        Stock.StockType expectedType = Stock.StockType.PREFERRED;
        String expectedStockId = stockId;
        Double expectedFixedDividend = fixedDividend;
        Integer expectedParValue = parValue;
        Double expectedTickerPrice = tickerPrice;

        Stock.StockType obtainedType = stock.getType();
        String obtainedId = stock.getId();
        Double obtainedFixedDividend = stock.getFixedDividend();
        Integer obtainedParValue = stock.getParValue();
        Double obtainedTickerPrice = stock.getTickerPrice();

        assertEquals("Unexpected stock type was found", expectedType, obtainedType);
        assertEquals("Unexpected stock id was found", expectedStockId, obtainedId);
        assertEquals("Unexpected stock fixed dividend was found", expectedFixedDividend, obtainedFixedDividend);
        assertEquals("Unexpected stock par value was found", expectedParValue, obtainedParValue);
        assertEquals("Unexpected stock price was found", expectedTickerPrice, obtainedTickerPrice);
    }

    @Test
    public void calculateDividendYieldTest(){

        final Double expectedDividendYield = (fixedDividend * parValue) / tickerPrice;
        final Double obtainedDividendYield = stock.calculateDividendYield();

        assertEquals("Unexpected dividend yield was found", expectedDividendYield, obtainedDividendYield);
    }

    @Test
    public void calculatePERatioTest(){

        final Double expectedDividendYield = (fixedDividend * parValue) / tickerPrice;
        final Double expectedPERatio = tickerPrice / expectedDividendYield;
        final Double obtainedPERatio = stock.calculatePERatio();

        assertEquals("Unexpected PERatio was found", expectedPERatio, obtainedPERatio);
    }

    @Test
    public void addAndRetrieveTradesTest(){
        super.addAndRetrieveTradesTest(stock);
    }

    @Test
    public void calculateTradePricesTest() throws ParseException {
        super.calculateTradePricesTest(stock);
    }
}
