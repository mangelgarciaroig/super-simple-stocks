package org.miguelangelgarciaroig.supersimplestoks.domain.entity;

import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TradeCollectionTest {

    @Test
    public void addAndRetrieveTradesTest(){

        final Date now = new Date();

        final TradeCollection tradeCollection = new TradeCollection();

        final Trade firstTrade = new Trade(Trade.TradeType.BUY, now, 2, 10.0);
        final Trade secondTrade = new Trade(Trade.TradeType.SELL, now, 1, 5.0);

        final Trade[] allTrades = {firstTrade, secondTrade};

        for (final Trade currentTrade : allTrades){
            tradeCollection.add(currentTrade);
        }

        final Collection<Trade> trades = tradesFrom(tradeCollection.iterator());

        int expectedNumberOfTrades = allTrades.length;
        int obtainedNumberOfTrades = tradeCollection.count();

        assertEquals("Unexpected number of trades in stock was found", expectedNumberOfTrades, obtainedNumberOfTrades);
        assertTrue("Expected trade to be contained in stock was found", trades.contains(firstTrade));
        assertTrue("Expected trade to be contained in stock was found", trades.contains(secondTrade));
    }

    @Test
    public void multiplyAllPricesTest(){

        final Date now = new Date();

        final TradeCollection tradeCollection = new TradeCollection();

        final Trade firstTrade = new Trade(Trade.TradeType.BUY, now, 2, 10.0);
        final Trade secondTrade = new Trade(Trade.TradeType.SELL, now, 1, 5.0);

        tradeCollection
                .add(firstTrade)
                .add(secondTrade);

        final Double expectedMultipliedPrices = firstTrade.calculateTotalPrice() * secondTrade.calculateTotalPrice();
        final Double obtainedMultipliedPrices = tradeCollection.multiplyAllPrices();

        assertEquals("Unexpected trades price multiplication was found", expectedMultipliedPrices, obtainedMultipliedPrices);
    }

    @Test
    public void isEmptyTest(){

        final TradeCollection tradeCollection = new TradeCollection();

        assertTrue("Unexpected non empty trade collection was found", tradeCollection.isEmpty());

        tradeCollection.add(new Trade(Trade.TradeType.BUY, new Date(), 2, 10.0));

        assertFalse("Unexpected empty trade collection was found", tradeCollection.isEmpty());
    }

    @Test
    public void calculatePricesTest() throws ParseException {

        final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        final Date firstDate = formatter.parse("2016-01-01 00:00:00");
        final Date secondDate = formatter.parse("2016-01-01 12:00:00");
        final Date thirdDate = formatter.parse("2016-01-02 05:00:00");
        final Date fourthDate = formatter.parse("2016-01-02 23:00:00");
        final Date fifthDate = formatter.parse("2016-01-02 23:59:59");

        final Trade firstTrade = new Trade(Trade.TradeType.BUY, firstDate, 1, 12.3);
        final Trade secondTrade = new Trade(Trade.TradeType.SELL, secondDate, 23, 50.3);
        final Trade thirdTrade = new Trade(Trade.TradeType.BUY, thirdDate, 4, 100.2);
        final Trade fourthTrade = new Trade(Trade.TradeType.SELL, fourthDate, 2, 23.1);
        final Trade fifthTrade = new Trade(Trade.TradeType.BUY, fifthDate, 8, 1.2);

        final TradeCollection tradeCollection = new TradeCollection()
                .add(firstTrade)
                .add(secondTrade)
                .add(thirdTrade)
                .add(fourthTrade)
                .add(fifthTrade);

        final Double expectedPrice = calculateExpectedPrice(secondTrade, thirdTrade, fourthTrade);
        final Double obtainedPrice = tradeCollection.calculatePrice(secondDate, fourthDate);

        assertEquals("Unexpected total price", expectedPrice, obtainedPrice);
    }

    private double calculateExpectedPrice(final Trade ...trades) {

        double totalUnweightedPrice = 0.0;
        double totalQuantity = 0.0;

        for (final Trade currentTrade : trades){
            totalUnweightedPrice+= currentTrade.getUnitPrice() * currentTrade.getQuantityOfShares();
            totalQuantity+= currentTrade.getQuantityOfShares();
        }

        return totalUnweightedPrice / totalQuantity;
    }

    private Collection<Trade> tradesFrom(final Iterator<Trade> tradesIterator){

        final Collection<Trade> trades = new ArrayList<>();

        while (tradesIterator.hasNext()){
            trades.add(tradesIterator.next());
        }

        return trades;
    }
}
