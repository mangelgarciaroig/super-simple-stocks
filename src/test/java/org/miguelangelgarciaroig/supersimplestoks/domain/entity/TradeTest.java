package org.miguelangelgarciaroig.supersimplestoks.domain.entity;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TradeTest {

    private final Date timestamp = new Date();
    private final int quantityOfShares = 10;
    private final double unitPrice = 2.0;

    private final Trade buyTrade = new Trade(Trade.TradeType.BUY, timestamp, quantityOfShares, unitPrice);
    private final Trade sellTrade = new Trade(Trade.TradeType.SELL, timestamp, quantityOfShares, unitPrice);

    @Test
    public void constructorTest(){

        assertEquals("Unexpected trade type was found", Trade.TradeType.BUY, buyTrade.getType());
        assertEquals("Unexpected trade type was found", Trade.TradeType.SELL, sellTrade.getType());

        assertEquals("Unexpected timestamp was found", buyTrade.getTimestamp(), buyTrade.getTimestamp());
        assertEquals("Unexpected timestamp was found", sellTrade.getTimestamp(), sellTrade.getTimestamp());

        assertEquals("Unexpected quantity was found", buyTrade.getQuantityOfShares(), buyTrade.getQuantityOfShares());
        assertEquals("Unexpected quantity was found", sellTrade.getQuantityOfShares(), sellTrade.getQuantityOfShares());

        assertEquals("Unexpected unit price was found", buyTrade.getUnitPrice(), buyTrade.getUnitPrice());
        assertEquals("Unexpected unit price was found", sellTrade.getUnitPrice(), sellTrade.getUnitPrice());
    }

    @Test
    public void calculateTotalPriceTest(){

        final Double expectedTotalPrice = quantityOfShares * unitPrice;

        final Double obtainedBuyTotalPrice = buyTrade.calculateTotalPrice();
        final Double obtainedSellTotalPrice = sellTrade.calculateTotalPrice();

        assertEquals("Unexpected total price for a buy trade", expectedTotalPrice, obtainedBuyTotalPrice);
        assertEquals("Unexpected total price for a sell trade", expectedTotalPrice, obtainedSellTotalPrice);
    }

    @Test
    public void equalsTest(){

        final Trade otherBuyTrade = new Trade(Trade.TradeType.BUY, timestamp, quantityOfShares, unitPrice);
        final Trade otherSellTrade = new Trade(Trade.TradeType.SELL, timestamp, quantityOfShares, unitPrice);

        assertTrue("Expected equals buy trade objects", buyTrade.equals(otherBuyTrade));
        assertTrue("Expected equals sell trade objects", sellTrade.equals(otherSellTrade));
        assertFalse("Expected distinct trade objects", otherSellTrade.equals(otherBuyTrade));
    }

    @Test
    public void cloneTest(){

        final Trade otherBuyTrade = buyTrade.clone();
        final Trade otherSellTrade = sellTrade.clone();

        assertTrue("Expected equals buy trade objects", buyTrade.equals(otherBuyTrade));
        assertTrue("Expected equals sell trade objects", sellTrade.equals(otherSellTrade));

        assertFalse("Expected distinct trade objects", otherSellTrade.equals(otherBuyTrade));

        assertTrue("Expected distinct trade references after clone operation", otherBuyTrade != buyTrade);
        assertTrue("Expected distinct trade references after clone operation", otherSellTrade != sellTrade);
    }
}
