package org.miguelangelgarciaroig.supersimplestoks.domain.service;

import org.junit.Before;
import org.junit.Test;
import org.miguelangelgarciaroig.supersimplestoks.domain.entity.CommonStock;
import org.miguelangelgarciaroig.supersimplestoks.domain.entity.Stock;
import org.miguelangelgarciaroig.supersimplestoks.domain.entity.Trade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GBCEShareIndexServiceTest {

    private GBCEShareIndexService service;

    @Before
    public void setUp(){
        service = new GBCEShareIndexService();
    }

    @Test
    public void calculateSharedServiceWithInputData(){

        final Trade firstTrade = new Trade(Trade.TradeType.BUY, new Date(), 2, 3.2);
        final Trade secondTrade = new Trade(Trade.TradeType.SELL, new Date(), 4, 1.3);

        final Stock firstStock = new CommonStock("stock-1", 10, 20, 3.2).addTrade(firstTrade);
        final Stock secondtStock = new CommonStock("stock-2", 11, 5, 4.2).addTrade(secondTrade);

        final List<Stock> allStocks = Arrays.asList(new Stock[] {firstStock, secondtStock});

        Double expectedShareIndex = Math.sqrt(firstTrade.calculateTotalPrice() * secondTrade.calculateTotalPrice());
        Double obtainedShareIndex = service.calculateShareIndex(allStocks);

        assertEquals("Unexpected share index when no trade data is available", expectedShareIndex, obtainedShareIndex);
    }

    @Test
    public void calculateSharedServiceWithoutInputData(){

        Double expectedShareIndex = 0.0;
        Double obtainedShareIndex = service.calculateShareIndex(new ArrayList<Stock>());

        assertEquals("Unexpected share index when no trade data is available", expectedShareIndex, obtainedShareIndex);
    }
}
